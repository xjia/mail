Send Email Using PHP `mail()` Function
======================================

Ubuntu 12.04 LTS (64 bit) -- Just after installation

Default MTA in Debian is `exim4` according to `ll /usr/sbin/sendmail` and `man sendmail`. Replace it with `postfix` first. Also install `php-mail` package (not sure but works).
```shell
sudo apt-get install php-mail postfix
```
Postfix installation will remove `exim4` automatically.

Then edit `php.ini`, e.g. `/etc/php5/cli/php.ini`.
```ini
sendmail_path = /usr/sbin/sendmail -t -i
mail.log = /tmp/mail.log
```

Then the following two scripts should work now. Remember to put real email addresses in the scripts.
```shell
php -f ex2.php
php -f ex4.php
```


Send email in UTF-8
-------------------
[](http://www.php.net/manual/en/function.mail.php#108669)
```php
<?php
function mail_utf8($to, $from_user, $from_email, $subject = '(No subject)', $message = '')
{
  $from_user = "=?UTF-8?B?".base64_encode($from_user)."?=";
  $subject = "=?UTF-8?B?".base64_encode($subject)."?=";
  $headers = "From: $from_user <$from_email>\r\n".
             "MIME-Version: 1.0" . "\r\n" .
             "Content-type: text/html; charset=UTF-8" . "\r\n";
  return mail($to, $subject, $message, $headers);
}
?>
```


UTF-8 HTML email for Outlook
-----------------------
[](http://www.php.net/manual/en/function.mail.php#108328)

If you want to send UTF-8 HTML letter you need to mention charset twice:

1) In message header:
```php
<?php
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
?>
```

2) In HTML header:
```php
<?php
$message = '
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <title>Fillon soutient à fond le retour d\'un Grand Prix de France</title>
</head>
<body>
   <p>Le Premier ministre François Fillon, passionné d\'automobile et pilote à ses heures, a apporté un soutien appuyé au retour d\'un Grand Prix de France au calendrier 2013 de la Formule 1, en faisant un passage-éclair vendredi sur le circuit Paul Ricard dans le Var.</p>
</body>
</html>
';
?>
```

In this case Outlook will also "understand" that message is encoded using UTF-8.


Clean PHP code for adding headers
---------------------------------
[](http://www.php.net/manual/en/function.mail.php#108368)
```php
<?php
$headers   = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/plain; charset=iso-8859-1";
$headers[] = "From: Sender Name <sender@domain.com>";
$headers[] = "Bcc: JJ Chong <bcc@domain2.com>";
$headers[] = "Reply-To: Recipient Name <receiver@domain3.com>";
$headers[] = "Subject: {$subject}";
$headers[] = "X-Mailer: PHP/".phpversion();

mail($to, $subject, $email, implode("\r\n", $headers));
?>
```


